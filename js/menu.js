
/*
    Author: Yu Zhao
    Copyright: Yu Zhao
*/

$(document).ready(function() {
      //Initialize the dropdown menu
      $(".dropdown-button").dropdown();
      
      // Initialize collapse button
      $(".button-collapse").sideNav({
        closeOnClick: true
      });
      $('.collapsible').collapsible();
      //get data
      $.ajax({                        //get data from json file
        url: "../data/menu.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "GET"
        }).success (function (data) {   //when success,render DOM with the data got from json file, this part should be modified
            var forEachelement = function(element, index, list){
                $("div#appetizer").append("<div id='' class='col s6 m3 l2 z-depth-3'></div>");//append div as container for each item.
                $("div#appetizer").children().last().attr("id", "appetizerContainer"+index);//set different id for each item container.
                $("div#appetizerContainer"+index).append("<ul id='' class='attribute center-align'></ul>");//append ul as container for each item
                $("div#appetizerContainer"+index).children().last().attr("id", "appetizerUl"+index);//set different id for each ul
                $("ul#appetizerUl"+index).append("<li><img id='' class='responsive-img thumbimg'></li>");//append li as container in ul for item image
                $("img:last").attr("id", "appetizerImg"+index);//set id for item image
                $("img#appetizerImg"+index).attr("src", element.img);//set src for each image
                $("ul#appetizerUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item name.
                $("ul#appetizerUl"+index).children().last().attr("id", "appetizerName"+index);//set id for each itemName container.
                $("li#appetizerName"+index).children().last().text(element.itemName);//set item name
                $("ul#appetizerUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item price.
                $("ul#appetizerUl"+index).children().last().attr("id", "appetizerPrice"+index);//set id for each itemPrice container.
                $("li#appetizerPrice"+index).children().last().text(element.itemPrice);//set item Price
            };
        _.each(data.appetizer, forEachelement);
        }).error(function (data, err) {                 //when error happens, log the error
            console.log("no data has been got!and the error message is "+ err);
        });
    //soup & salad
    $.ajax({                        //get data from json file
        url: "../data/menu.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "GET"
        }).success (function (data) {   //when success,render DOM with the data got from json file, this part should be modified
            var forEachelement = function(element, index, list){
                $("div#SoupAndSalad").append("<div id='' class='col s6 m3 l2 menuFormat z-depth-3'></div>");//append div as container for each item.
                $("div#SoupAndSalad").children().last().attr("id", "SoupAndSaladContainer"+index);//set different id for each item container.
                $("div#SoupAndSaladContainer"+index).append("<ul id='' class='center attribute'></ul>");//append ul as container for each item
                $("div#SoupAndSaladContainer"+index).children().last().attr("id", "SoupAndSaladUl"+index);//set different id for each ul
                $("ul#SoupAndSaladUl"+index).append("<li><img id='' class='responsive-img thumbimg'></li>");//append li as container in ul for item image
                $("img:last").attr("id", "SoupAndSaladImg"+index);//set id for item image
                $("img#SoupAndSaladImg"+index).attr("src", element.img);//set src for each image
                $("ul#SoupAndSaladUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item name.
                $("ul#SoupAndSaladUl"+index).children().last().attr("id", "SoupAndSaladName"+index);//set id for each itemName container.
                $("li#SoupAndSaladName"+index).children().last().text(element.itemName);//set item name
                $("ul#SoupAndSaladUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item price.
                $("ul#SoupAndSaladUl"+index).children().last().attr("id", "SoupAndSaladPrice"+index);//set id for each itemPrice container.
                $("li#SoupAndSaladPrice"+index).children().last().text(element.itemPrice);//set item Price
            };
        _.each(data.soupAndsalad, forEachelement);
        }).error(function (data, err) {                 //when error happens, log the error
            console.log("no data has been got!and the error message is "+ err);
        });
    //rice & noodle &udon
     $.ajax({                        //get data from json file
        url: "../data/menu.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "GET"
        }).success (function (data) {   //when success,render DOM with the data got from json file, this part should be modified
            var forEachelement = function(element, index, list){
                $("div#RiceAndUdonAndNoodle").append("<div id='' class='col s6 m3 l2 menuFormat z-depth-3'></div>");//append div as container for each item.
                $("div#RiceAndUdonAndNoodle").children().last().attr("id", "RiceAndUdonAndNoodleContainer"+index);//set different id for each item container.
                $("div#RiceAndUdonAndNoodleContainer"+index).append("<ul id='' class='center attribute'></ul>");//append ul as container for each item
                $("div#RiceAndUdonAndNoodleContainer"+index).children().last().attr("id", "RiceAndUdonAndNoodleUl"+index);//set different id for each ul
                $("ul#RiceAndUdonAndNoodleUl"+index).append("<li><img id='' class='responsive-img thumbimg'></li>");//append li as container in ul for item image
                $("img:last").attr("id", "RiceAndUdonAndNoodleImg"+index);//set id for item image
                $("img#RiceAndUdonAndNoodleImg"+index).attr("src", element.img);//set src for each image
                $("ul#RiceAndUdonAndNoodleUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item name.
                $("ul#RiceAndUdonAndNoodleUl"+index).children().last().attr("id", "RiceAndUdonAndNoodleName"+index);//set id for each itemName container.
                $("li#RiceAndUdonAndNoodleName"+index).children().last().text(element.itemName);//set item name
                $("ul#RiceAndUdonAndNoodleUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item price.
                $("ul#RiceAndUdonAndNoodleUl"+index).children().last().attr("id", "RiceAndUdonAndNoodlePrice"+index);//set id for each itemPrice container.
                $("li#RiceAndUdonAndNoodlePrice"+index).children().last().text(element.itemPrice);//set item Price
            };
        _.each(data.RiceAndUdonAndNoodle, forEachelement);
        }).error(function (data, err) {                 //when error happens, log the error
            console.log("no data has been got!and the error message is "+ err);
        });
    //sashimi
     $.ajax({                        //get data from json file
        url: "../data/menu.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "GET"
        }).success (function (data) {   //when success,render DOM with the data got from json file, this part should be modified
            var forEachelement = function(element, index, list){
                $("div#Sashimi").append("<div id='' class='col s6 m4 l2 menuFormat z-depth-3'></div>");//append div as container for each item.
                $("div#Sashimi").children().last().attr("id", "SashimiContainer"+index);//set different id for each item container.
                $("div#SashimiContainer"+index).append("<ul id='' class='center attribute'></ul>");//append ul as container for each item
                $("div#SashimiContainer"+index).children().last().attr("id", "SashimiUl"+index);//set different id for each ul
                $("ul#SashimiUl"+index).append("<li><img id='' class='responsive-img thumbimg'></li>");//append li as container in ul for item image
                $("img:last").attr("id", "SashimiImg"+index);//set id for item image
                $("img#SashimiImg"+index).attr("src", element.img);//set src for each image
                $("ul#SashimiUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item name.
                $("ul#SashimiUl"+index).children().last().attr("id", "SashimiName"+index);//set id for each itemName container.
                $("li#SashimiName"+index).children().last().text(element.itemName);//set item name
                $("ul#SashimiUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item price.
                $("ul#SashimiUl"+index).children().last().attr("id", "SashimiPrice"+index);//set id for each itemPrice container.
                $("li#SashimiPrice"+index).children().last().text(element.itemPrice);//set item Price
            };
        _.each(data.Sashimi, forEachelement);
        }).error(function (data, err) {                 //when error happens, log the error
            console.log("no data has been got!and the error message is "+ err);
        });
        //sushi
     $.ajax({                        //get data from json file
        url: "../data/menu.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "GET"
        }).success (function (data) {   //when success,render DOM with the data got from json file, this part should be modified
            var forEachelement = function(element, index, list){
                $("div#Sushi").append("<div id='' class='col s6 m4 l2 menuFormat z-depth-3'></div>");//append div as container for each item.
                $("div#Sushi").children().last().attr("id", "SushiContainer"+index);//set different id for each item container.
                $("div#SushiContainer"+index).append("<ul id='' class='attribute'></ul> ");//append ul as container for each item
                $("div#SushiContainer"+index).children().last().attr("id", "SushiUl"+index);//set different id for each ul
                $("ul#SushiUl"+index).append("<img id='' class='responsive-img thumbimg'>");//append li as container in ul for item image
                $("img:last").attr("id", "SushiImg"+index);//set id for item image
                $("img#SushiImg"+index).attr("src", element.img);//set src for each image
                $("ul#SushiUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item name.
                $("ul#SushiUl"+index).children().last().attr("id", "SushiName"+index);//set id for each itemName container.
                $("li#SushiName"+index).children().last().text(element.itemName);//set item name
                $("ul#SushiUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item price.
                $("ul#SushiUl"+index).children().last().attr("id", "SushiPrice"+index);//set id for each itemPrice container.
                $("li#SushiPrice"+index).children().last().text(element.itemPrice);//set item Price
            };
        _.each(data.Sushi, forEachelement);
        }).error(function (data, err) {                 //when error happens, log the error
            console.log("no data has been got!and the error message is "+ err);
        });
        //Bento Box
     $.ajax({                        //get data from json file
        url: "../data/menu.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "GET"
        }).success (function (data) {   //when success,render DOM with the data got from json file, this part should be modified
            var forEachelement = function(element, index, list){
                $("div#BentoBox").append("<div id='' class='col s6 m4 l2 menuFormat z-depth-3'></div>");//append div as container for each item.
                $("div#BentoBox").children().last().attr("id", "BentoBoxContainer"+index);//set different id for each item container.
                $("div#BentoBoxContainer"+index).append("<ul id='' class='cneter attribute'></ul>");//append ul as container for each item
                $("div#BentoBoxContainer"+index).children().last().attr("id", "BentoBoxUl"+index);//set different id for each ul
                $("ul#BentoBoxUl"+index).append("<li><img id='' class='responsive-img thumbimg'></li>");//append li as container in ul for item image
                $("img:last").attr("id", "BentoBoxImg"+index);//set id for item image
                $("img#BentoBoxImg"+index).attr("src", element.img);//set src for each image
                $("ul#BentoBoxUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item name.
                $("ul#BentoBoxUl"+index).children().last().attr("id", "BentoBoxName"+index);//set id for each itemName container.
                $("li#BentoBoxName"+index).children().last().text(element.itemName);//set item name
                $("ul#BentoBoxUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item price.
                $("ul#BentoBoxUl"+index).children().last().attr("id", "BentoBoxPrice"+index);//set id for each itemPrice container.
                $("li#BentoBoxPrice"+index).children().last().text(element.itemPrice);//set item Price
            };
        _.each(data.BentoBox, forEachelement);
        }).error(function (data, err) {                 //when error happens, log the error
            console.log("no data has been got!and the error message is "+ err);
        });
        //House special roll
     $.ajax({                        //get data from json file
        url: "../data/menu.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "GET"
        }).success (function (data) {   //when success,render DOM with the data got from json file, this part should be modified
            var forEachelement = function(element, index, list){
                $("div#HouseSpecialRoll").append("<div id='' class='col s6 m4 l2 menuFormat z-depth-3'></div>");//append div as container for each item.
                $("div#HouseSpecialRoll").children().last().attr("id", "HouseSpecialRollContainer"+index);//set different id for each item container.
                $("div#HouseSpecialRollContainer"+index).append("<ul id='' class='attribute'></ul>");//append ul as container for each item
                $("div#HouseSpecialRollContainer"+index).children().last().attr("id", "HouseSpecialRollUl"+index);//set different id for each ul
                $("ul#HouseSpecialRollUl"+index).append("<li><img id='' class='responsive-img thumbimg'></li>");//append li as container in ul for item image
                $("img:last").attr("id", "HouseSpecialRollImg"+index);//set id for item image
                $("img#HouseSpecialRollImg"+index).attr("src", element.img);//set src for each image
                $("ul#HouseSpecialRollUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item name.
                $("ul#HouseSpecialRollUl"+index).children().last().attr("id", "HouseSpecialRollName"+index);//set id for each itemName container.
                $("li#HouseSpecialRollName"+index).children().last().text(element.itemName);//set item name
                $("ul#HouseSpecialRollUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item price.
                $("ul#HouseSpecialRollUl"+index).children().last().attr("id", "HouseSpecialRollPrice"+index);//set id for each itemPrice container.
                $("li#HouseSpecialRollPrice"+index).children().last().text(element.itemPrice);//set item Price
            };
        _.each(data.HouseSpecialRoll, forEachelement);
        }).error(function (data, err) {                 //when error happens, log the error
            console.log("no data has been got!and the error message is "+ err);
        });
         //Sushi Platters
     $.ajax({                        //get data from json file
        url: "../data/menu.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "GET"
        }).success (function (data) {   //when success,render DOM with the data got from json file, this part should be modified
            var forEachelement = function(element, index, list){
                $("div#SushiPlatters").append("<div id='' class='col s6 m4 l2 menuFormat z-depth-3'></div>");//append div as container for each item.
                $("div#SushiPlatters").children().last().attr("id", "SushiPlattersContainer"+index);//set different id for each item container.
                $("div#SushiPlattersContainer"+index).append("<ul id='' class='center attribute'></ul>");//append ul as container for each item
                $("div#SushiPlattersContainer"+index).children().last().attr("id", "SushiPlattersUl"+index);//set different id for each ul
                $("ul#SushiPlattersUl"+index).append("<li><img id='' class='responsive-img thumbimg'></li>");//append li as container in ul for item image
                $("img:last").attr("id", "SushiPlattersImg"+index);//set id for item image
                $("img#SushiPlattersImg"+index).attr("src", element.img);//set src for each image
                $("ul#SushiPlattersUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item name.
                $("ul#SushiPlattersUl"+index).children().last().attr("id", "SushiPlattersName"+index);//set id for each itemName container.
                $("li#SushiPlattersName"+index).children().last().text(element.itemName);//set item name
                $("ul#SushiPlattersUl"+index).append("<li class='itemPropertyContainer'><p class='itemPropertyContainer' id=''></p></li>");//append p as container for item price.
                $("ul#SushiPlattersUl"+index).children().last().attr("id", "SushiPlattersPrice"+index);//set id for each itemPrice container.
                $("li#SushiPlattersPrice"+index).children().last().text(element.itemPrice);//set item Price
            };
        _.each(data.SushiPlatters, forEachelement);
        }).error(function (data, err) {                 //when error happens, log the error
            console.log("no data has been got!and the error message is "+ err);
        });
});

