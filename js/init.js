(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.slider').slider({full_width: true});
    //carousel-slider, added autoplay
    $('.carousel.carousel-slider').carousel({full_width: true});
    autoplay()   
    function autoplay() {
    $('.carousel').carousel('next');
    setTimeout(autoplay, 7000);
    }
    $('.gmap', window.parent.document).height('300px');


  }); // end of document ready
})(jQuery); // end of jQuery name space